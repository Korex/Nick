package net.korex.nickapi;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Skin {

	String uuid;
	String name;
	String value;
	String signatur;

	public Skin(String uuid) {
		this.uuid = uuid;
		this.load();
	}

	private void load() {
		try {
			// Get the name from SwordPVP
			URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
			URLConnection uc = url.openConnection();
			uc.setUseCaches(false);
			uc.setDefaultUseCaches(false);
			uc.addRequestProperty("User-Agent", "Mozilla/5.0");
			uc.addRequestProperty("Cache-Control", "no-cache, no-store, must-revalidate");
			uc.addRequestProperty("Pragma", "no-cache");

			// Parse it
			String json = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A").next();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(json);
			JSONArray properties = (JSONArray) ((JSONObject) obj).get("properties");
			for (int i = 0; i < properties.size(); i++) {
				try {
					JSONObject property = (JSONObject) properties.get(i);
					String name = (String) property.get("name");
					String value = (String) property.get("value");
					String signature = property.containsKey("signature") ? (String) property.get("signature") : null;

					this.name = name;
					this.value = value;
					this.signatur = signature;

				} catch (Exception e) {
					System.out.println("Failed to apply auth property");
				}
			}
		} catch (Exception e) {
			; // Failed to load skin
		}
	}

	public String getSkinValue() {
		return value;
	}

	public String getSkinName() {
		return name;
	}

	public String getSkinSignatur() {
		return signatur;
	}

}
