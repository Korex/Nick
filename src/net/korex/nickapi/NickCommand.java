package net.korex.nickapi;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NickCommand implements CommandExecutor {

	// /nick <Player> <Name/remove>

	@Override
	public boolean onCommand(CommandSender sender, Command paramCommand, String paramString, String[] args) {
		
		if (!sender.hasPermission(Main.permission)) {
			sender.sendMessage(Main.pr + "§cDu hast dafür keine Berechtigung!");
			return true;
		}

		if (args.length == 2) {

			Player target = Bukkit.getPlayer(args[0]);
			
			String nickname = args[1];

			if (target != null) {
				Nick n = new Nick(target);

				if(nickname.equalsIgnoreCase("remove")) {
					if(Nick.getUnnickedName(target) == null) {
						sender.sendMessage(Main.pr+"§cDer Spieler ist nicht genickt!");
					} else {
						n.removeNick();
						sender.sendMessage(Main.pr + "Du hast den Nicknamen vom Spieler entfernt!");
					}
				} else {
					if (nickname.length() < 17) {
						n.changeNick(nickname);
						sender.sendMessage(Main.pr + "Du hast den Spieler genickt!");
					} else {
						sender.sendMessage(Main.pr + "§cDer Name darf max. 16 Zeichen sein!");
					}
				}
				
			} else {
				sender.sendMessage(Main.pr + "§cDer Spieler ist nicht online!");
			}

		} else {
			sender.sendMessage(Main.pr + "/nick <Playername> <Name/Remove>");
		}

		return true;
	}

}
