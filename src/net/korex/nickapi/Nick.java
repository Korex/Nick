package net.korex.nickapi;

import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_9_R2.CraftServer;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_9_R2.EntityPlayer;
import net.minecraft.server.v1_9_R2.MinecraftServer;
import net.minecraft.server.v1_9_R2.Packet;
import net.minecraft.server.v1_9_R2.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_9_R2.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_9_R2.PlayerConnection;
import net.minecraft.server.v1_9_R2.PlayerInteractManager;
import net.minecraft.server.v1_9_R2.WorldServer;

public class Nick {

	private Player p;

	private static HashMap<Player, String> nicks;

	static {
		nicks = new HashMap<>();
	}

	public Nick(Player p) {
		this.p = p;
	}

	public static String getUnnickedName(Player p) {
		if (nicks.containsKey(p)) {
			return nicks.get(p);
		}
		return null;
	}

	public void changeNick(String nick) {

		MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
		WorldServer nmsWorld = ((CraftWorld) this.p.getWorld()).getHandle();
		PlayerInteractManager manager = new PlayerInteractManager(nmsWorld);
		GameProfile profile = new GameProfile(this.p.getUniqueId(), nick);

		EntityPlayer nickedPlayer = new EntityPlayer(nmsServer, nmsWorld, profile, manager);
		EntityPlayer unnickedPlayer = ((CraftPlayer) this.p).getHandle();

		this.refresh(unnickedPlayer, nickedPlayer);

		nicks.put(this.p, this.p.getName());
	}

	public void changeAll(UUID uuidTarget, String name) {
		CraftPlayer cp = ((CraftPlayer) p);
		GameProfile profile = cp.getProfile();
		GameProfile game = new GameProfile(uuidTarget, name);

		Collection<Property> col = profile.getProperties().get("textures");

		profile.getProperties().putAll("textures", col);
		

	}

	public void removeNick() {

		if (Nick.getUnnickedName(this.p) != null) {

			String realname = Nick.getUnnickedName(this.p);
			MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
			WorldServer nmsWorld = ((CraftWorld) this.p.getWorld()).getHandle();
			PlayerInteractManager manager = new PlayerInteractManager(nmsWorld);

			GameProfile profileNicked = new GameProfile(this.p.getUniqueId(), this.p.getCustomName());
			GameProfile profileUnnicked = new GameProfile(this.p.getUniqueId(), realname);

			EntityPlayer nickedPlayer = new EntityPlayer(nmsServer, nmsWorld, profileNicked, manager);
			EntityPlayer unnickedPlayer = new EntityPlayer(nmsServer, nmsWorld, profileUnnicked, manager);

			this.refresh(nickedPlayer, unnickedPlayer);
		}

	}

	private void refresh(EntityPlayer removePlayer, EntityPlayer spawnPlayer) {
		PacketPlayOutPlayerInfo remove = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, removePlayer);
		PacketPlayOutPlayerInfo spawn = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, spawnPlayer);

		this.sendPacket(remove);
		sendPacket(spawn);
	}

	private void sendPacket(Packet<?> packet) {

		for (Player players : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) players).getHandle().playerConnection.sendPacket(packet);
		}
	}

}
